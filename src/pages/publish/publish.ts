import { Component, OnInit } from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import { FormControl, FormGroup, Validators} from '@angular/forms';
import { PublishService } from "../../providers/publish-service";
import { Convocatoria } from "../../models/convocatoria";
import {PostData} from "../../providers/post-data";
import {Util} from "../../util/util";
import {Helpers} from "../../util/helpers";
import {HttpErrorResponse} from "@angular/common/http";
import { constants } from '../../app.constants';
import { User } from '../../models/user';
import {Storage} from "@ionic/storage";

@IonicPage()
@Component({
	selector: 'page-publish',
	templateUrl: 'publish.html',
})
export class PublishPage implements OnInit {

	convocatoria: Convocatoria;
	publishForm: FormGroup;
	especialidades: IEspecialidad[] = [];
	ciudades: any[];
	action: string;

  	constructor(
		public navCtrl: NavController,
		public publishService: PublishService,
		private navParams: NavParams,
		private loadingCtrl: LoadingController,
		private helper: Helpers,
		private postService: PostData,
		private storage: Storage
	) {
  		this.ciudades = [];
  		this.action = 'Crear';
	}

	ngOnInit(): void {
		if (this.navParams.data.post_id) {
			const convocatorias: Convocatoria[] = this.postService.posts_cache;
			convocatorias.forEach((convocatoria: Convocatoria) => {
				if (convocatoria.id == this.navParams.data.post_id) {
					this.convocatoria = convocatoria;
					this.convocatoria.fecha_inicio = new Date(this.convocatoria.fecha_inicio).toISOString();
					this.convocatoria.fecha_fin = new Date(this.convocatoria.fecha_fin).toISOString();
					this.action = 'Actualizar';
					return;
				}
			});
		}

		if (!this.convocatoria) {
			this.convocatoria = Convocatoria.createBlank();
		}

		this.publishForm = this.createPublishForm(this.convocatoria.id > 0);

		this.checkEspecialidades();
	}

  	saveData(): void {
  		let contentMessage: string = this.convocatoria.id > 0
			? 'Actualizando convocatoria....'
			: 'Creando convocatoria...';
  		let loading = this.loadingCtrl.create({content: contentMessage});
		loading.present();

		this.storage.get(constants.KEY_USER).then((user: User) => {
			this.convocatoria.user_id = user.id;

			this.publishService.createConvocatoria(this.convocatoria).subscribe(
				(data: Convocatoria) => {
					loading.dismiss();
					if (this.convocatoria.id > 0) {
						this.postService.updatePostCache(data);
						this.helper.customAlert('Convocatoria actualizada.', 'Enhorabuena!').present();
					} else {
						this.postService.posts_cache = [data];
						this.navCtrl.popToRoot();
					}
				},
				(error: HttpErrorResponse | string) => {
					if (error instanceof HttpErrorResponse) {
						const html = Util.createErrorAlert(error, this.convocatoria);
						this.helper.customAlert(html).present();
					}
					loading.dismiss();
				});
		});
	}

	private createPublishForm(isUpdate: boolean = false) {
  		let formArray: any = {
			title: new FormControl(this.convocatoria.title, []),
			content: new FormControl(this.convocatoria.content, []),
			email: new FormControl(this.convocatoria.email, [Validators.required, Validators.email]),
			requisitos: new FormControl(this.convocatoria.requisitos, Validators.required),
			telefono: new FormControl(this.convocatoria.telefono, Validators.required),
			modalidad: new FormControl(this.convocatoria.modalidad, Validators.required),
			especialidad: new FormControl(this.convocatoria.especialidad, isUpdate ? [] : Validators.required),
			sueldo: new FormControl(this.convocatoria.sueldo, []),
			num_vacante: new FormControl(this.convocatoria.num_vacante, [Validators.required, Validators.min(1), Validators.max(20)]),
			ciudad: new FormControl(this.convocatoria.ciudad, Validators.required),
			fecha_inicio: new FormControl(this.convocatoria.fecha_inicio, Validators.required),
			fecha_fin: new FormControl(this.convocatoria.fecha_fin, Validators.required),
			modo: new FormControl(this.convocatoria.modo, Validators.required),
			tags: new FormControl(this.convocatoria.tags, Validators.required),
		};
		return new FormGroup(formArray);
	}

	private checkEspecialidades(): void {
		this.publishService.checkIfEspecialidadesIsInCache().then((data: any) => {
			if (!data) {
				this.publishService.getEspecialidades()
					.subscribe(resource => {
						this.prepareData(resource);
					});
			} else {
				this.prepareData(data);
			}
		});
	}

	private prepareData(data: any): void {
		this.especialidades = data.especialidades as IEspecialidad[];
		this.ciudades = Util.getCiudades(data.ciudades);
	}
}
