import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PublishPage } from './publish';

@NgModule({
  imports: [
    IonicPageModule.forChild(PublishPage),
  ]
})
export class PublishPageModule {}

