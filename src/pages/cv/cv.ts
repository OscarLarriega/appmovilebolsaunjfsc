import { Component, OnInit } from '@angular/core';
import { AlertController, IonicPage, LoadingController, NavController} from 'ionic-angular';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Cv } from "../../models/cv";
import { User } from "../../models/user";
import { Storage } from "@ionic/storage";
import { constants } from "../../app.constants";
import { CvProvider } from "../../providers/cv/cv";
import {HttpErrorResponse} from "@angular/common/http";
import {PublishService} from "../../providers/publish-service";
import {Util} from "../../util/util";

@IonicPage()
@Component({
  selector: 'page-cv',
  templateUrl: 'cv.html',
})
export class CvPage implements OnInit {

	cv: Cv;
	cvForm: FormGroup;
	user: User;
	ciudades: any[];

	constructor(private navCtrl: NavController,
				private storage: Storage,
				private cvProvider: CvProvider,
				private loadingCtrl: LoadingController,
				private alertCtrl: AlertController,
				private publishService: PublishService) {
	}

	ngOnInit(): void {

		this.storage.get(constants.KEY_USER).then((user: User) => {
			this.user = user;
			this.getCvFromUserId(this.user.id);
		});
	}

	public saveData(): void {
		this.cv.user_id = this.user.id;
		const loading = this.loadingCtrl.create({content: 'Cargando...'});
		loading.present();
		this.cvProvider.createCv(this.cv)
			.then((cv: Cv) => {
				loading.dismiss();
				this.showAlert();
			});
	}

	private createCvForm(): FormGroup {
		return new FormGroup({
			perfil: new FormControl(this.cv.perfil, Validators.required),
			ciudad: new FormControl(this.cv.ciudad, Validators.required),
			edu_primaria: new FormControl(this.cv.edu_primaria, Validators.required),
			fec_fin_primaria: new FormControl(this.cv.fec_fin_primaria, Validators.required),
			edu_secundaria: new FormControl(this.cv.edu_secundaria, Validators.required),
			fec_fin_secundaria: new FormControl(this.cv.fec_fin_secundaria, Validators.required),
			edu_superior: new FormControl(this.cv.edu_superior, Validators.required),
			fec_fin_superior: new FormControl(this.cv.fec_fin_superior, []),
			exp_laboral: new FormControl(this.cv.exp_laboral, Validators.required),
			capacitaciones: new FormControl(this.cv.capacitaciones, Validators.required),
			sueldo: new FormControl(this.cv.sueldo, [Validators.required, Validators.min(1)]),
			idiomas: new FormControl(this.cv.idiomas, Validators.required)
		});
	}

	private getCvFromUserId(user_id: number): void {
		const loading = this.loadingCtrl.create({content: 'Cargando...'});
		loading.present();

		this.cvProvider.getCv(user_id)
			.subscribe(
				(cv: Cv) => {
					this.cv = cv;
					this.cvForm = this.createCvForm();
					loading.dismiss();
				},
				(error: HttpErrorResponse | string) => {
					loading.dismiss();
					this.cv = Cv.createBlank();
					this.cvForm = this.createCvForm();
				}
			);
		this.checkEspecialidades();
	}

	private showAlert(): void {
		const alert = this.alertCtrl.create({
			title: 'Curriculun Vitae',
			message: 'Cv Creado con éxito!',
			buttons: [{
				text: 'OK',
				handler: () => {
					this.navCtrl.popToRoot();
				}
			}]
		});
		alert.present();
	}

	private checkEspecialidades(): void {
		this.publishService.checkIfEspecialidadesIsInCache().then((data: any) => {
			if (!data) {
				this.publishService.getEspecialidades()
					.subscribe(resource => {
						this.prepareData(resource);
					});
			} else {
				this.prepareData(data);
			}
		});
	}

	private prepareData(data: any): void {
		this.ciudades = Util.getCiudades(data.ciudades);
	}
}
