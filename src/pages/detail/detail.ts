import {Component} from '@angular/core';
import { LoadingController, NavParams } from 'ionic-angular';
import { EmpresaDataProvider } from "../../providers/empresa-data";
import {Convocatoria} from "../../models/convocatoria";
import {constants} from "../../app.constants";
import {PostData} from "../../providers/post-data";
import {Postulate} from "../../interfaces/postulate";
import {Empresa} from "../../models/empresa";
import {Storage} from "@ionic/storage";
import {User} from "../../models/user";
import {HttpErrorResponse} from "@angular/common/http";
import {Helpers} from "../../util/helpers";

@Component({
	selector: 'page-detail',
	templateUrl: 'detail.html',
})
export class DetailPage {

	convocatoria: Convocatoria;
	empresa: Empresa = <Empresa> {photo: '', nombre_emp: 'Cargando...'};
	isPostulate: boolean;

	constructor(public navParams: NavParams,
				public empresaService: EmpresaDataProvider,
				private postService: PostData,
				private loadingCtrl: LoadingController,
				private storage: Storage,
				private helper: Helpers) {

		this.isPostulate = false;
		this.convocatoria = navParams.data.post as Convocatoria;

		this.getShortInfoEmpresa(navParams.data.post.empresa_id);
	}

	ionViewDidLoad() {
		for (const postulate of this.postService.postulations) {
			if (postulate.post_id == this.convocatoria.id) {
				this.isPostulate = true;
				return;
			}
		}
	}

	getShortInfoEmpresa(empresaId: number) {
		if (this.empresa.id !== empresaId) {
			this.empresa.photo = `${constants.URL_HOST}/docs/logo/${this.convocatoria['photo']}`;
			this.empresa.nombre_emp = this.convocatoria['nombre_emp'];
			this.empresa.correo = this.convocatoria['correo'];
			this.empresa.telefono = this.convocatoria['telefono'];
			this.empresa.id = empresaId;
		}
	}

	postulate(): void {
		this.storage.get(constants.KEY_USER).then((user: User) => {

			let postulate: Postulate = {
				user_id: user.id,
				post_id: this.convocatoria.id
			};
			let loading = this.loadingCtrl.create({content: 'Postulando...'});
			loading.present();

			this.postService.postulate(postulate)
				.subscribe((res: Postulate) => {
					this.postService.pushPostulation(res);
					this.isPostulate = true;
					loading.dismiss();
				}, (error: HttpErrorResponse | string) => {
					if (error instanceof HttpErrorResponse) {
						this.helper.customAlert(error.error.error_message).present();
					}
					loading.dismiss();
				});
		});
	}
}
