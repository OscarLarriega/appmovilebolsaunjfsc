import {Component, OnInit} from '@angular/core';
import { Storage } from "@ionic/storage";
import { PostData } from "../../providers/post-data";
import { AlertController, LoadingController, NavController, Platform } from 'ionic-angular';
import { DetailPage } from "../detail/detail";
import {FCM} from "@ionic-native/fcm";
import {Vibration} from "@ionic-native/vibration";
import {constants} from "../../app.constants";
import {User} from "../../models/user";
import {Convocatoria} from "../../models/convocatoria";
import { UserConfigPage } from '../user-config/user-config';
import { UserData } from '../../providers/user-data';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {

	posts: Array<any> = [];
	user: User = <User> {};

	constructor(private postService: PostData,
				private navCtrl: NavController,
				private loadingCtrl: LoadingController,
				private storage: Storage,
				private fcm: FCM,
				private alertCtrl: AlertController,
				private vibration: Vibration,
				private platform: Platform,
				private userService: UserData
	) {
		this.onNotification();
	}

	ngOnInit(): void {
		let loading = this.loadingCtrl.create({ content: 'Cargando ofertas...'});
		loading.present();

		this.storage.get(constants.KEY_USER).then((user: User) => {
			this.user = user;
			this.getPosts(user.especialidad_id);
			this.postService.getPostulations(user.id);
			loading.dismissAll();
		});

		this.userService.userUpdate.subscribe((user: User) => this.user = user);
	}

	getPosts(especialidad: number): void {
		this.postService.posts(especialidad).subscribe((data: any) => {
			this.posts = data.data;
		});
	}

	viewDetail(postId: number): void {
		this.posts.forEach((post: Convocatoria) => {
			if (post.id == postId) {
				this.navCtrl.push(DetailPage, {post: post});
				return;
			}
		});
	}

	goToConfig(): void {
		this.navCtrl.push(UserConfigPage);
	}

	getEmpresaLogo(photo: string): string {
		if (photo == null || photo == '') {
			photo = 'enterprise.png';
		}
		return `${constants.URL_HOST}/docs/logo/${photo}`;
	}

	async onNotification() {
		if (this.isPlatformCordova()) {
			this.fcm.onNotification().subscribe(data => {
				this.posts.unshift(data);
				if (data.wasTapped) {
					this.navCtrl.push(DetailPage, {post: data});
				} else {
					this.vibration.vibrate(1000);
					let confirm = this.alertCtrl.create({
						title: 'Nueva Publicación',
						message:data.title,
						buttons: [
							{
								text: 'Ver mas',
								handler: () => {
									this.navCtrl.push(DetailPage, {post: data});
								}
							}
						]
					});

					confirm.present();
				}
			})
		}
	}

	isPlatformCordova(): boolean {
		return this.platform.is('cordova');
	}
}
