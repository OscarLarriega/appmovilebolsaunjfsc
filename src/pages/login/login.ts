import {Component, OnDestroy} from '@angular/core';
import {Loading, LoadingController, NavController, Platform} from 'ionic-angular';
import {Storage} from "@ionic/storage";
import {UserData} from "../../providers/user-data";
import {HomePage} from "../home/home";
import {FCM} from "@ionic-native/fcm";
import {constants} from "../../app.constants";
import {User} from "../../models/user";
import {AdminHomePage} from "../admin-home/admin-home";
import {HttpErrorResponse} from "@angular/common/http";
import {Util} from "../../util/util";
import {Helpers} from "../../util/helpers";

@Component({
	selector: 'page-login',
	templateUrl: 'login.html'
})

export class LoginPage implements OnDestroy {

	error: any;
	user: User;
	errorMessages: string[] = [];

	constructor(
		public navCtrl: NavController,
		public userService: UserData,
		public storage: Storage,
		private loadingCtrl: LoadingController,
		private platform: Platform,
		private fcm: FCM,
		private helper: Helpers) {

		this.error = false;
		this.user = <User> {dni: '', password: ''};
	}

	public login(): void {
		let loading = this.loadingCtrl.create({
			content: 'Iniciando Sesión...'
		});
		loading.present();

		this.userService.login(this.user).subscribe(
			(data: User) => {
				this.storage.set(constants.KEY_USER, data);
				this.userService.emitUpdateUserData(data);
				this.getToken(data, loading);
			},
			(error: HttpErrorResponse | string) => {
				if (error instanceof HttpErrorResponse) {
					let html = Util.createErrorAlert(error, this.user);
					this.helper.customAlert(html).present();
				} else if (error !== null) {
					this.helper.customAlert(error).present();
				}
				loading.dismiss();
			});
		this.error = true;
	}

	getToken(user: User, loading: Loading) {
		if (!this.platform.is('cordova')) {
			loading.dismiss();
			this.goToHomePage(user);
			return;
		}

		if (parseInt(user.is_fcm_token) === 1) {
			loading.dismiss();
			this.goToHomePage(user);
			return;
		}
		this.fcm.getToken().then(token => {
			this.userService.saveToken(token, user).subscribe(() => {
				loading.dismiss();
				this.goToHomePage(user);
			});
		});
	}

	ngOnDestroy(): void {
		console.log('DESTROY');
	}

	private goToHomePage(user: User): void {
		if (user.tipo_id == constants.ASPIRANTE) {
			this.navCtrl.setRoot(HomePage);
		} else if (user.tipo_id == constants.EMPRESA) {
			this.navCtrl.setRoot(AdminHomePage);
		} else {
			this.userService.logout(user.id);
		}
	}
}
