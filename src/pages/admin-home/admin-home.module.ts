import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminHomePage } from './admin-home';

@NgModule({
  imports: [
    IonicPageModule.forChild(AdminHomePage),
  ],
})
export class AdminHomePageModule {}
