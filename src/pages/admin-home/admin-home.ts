import {Component, OnInit} from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';
import {PostData} from "../../providers/post-data";
import {Storage} from "@ionic/storage";
import {constants} from "../../app.constants";
import {User} from "../../models/user";
import {PublishPage} from "../publish/publish";
import { EmpresaConfigPage } from '../empresa-config/empresa-config';
import { EmpresaDataProvider } from '../../providers/empresa-data';

@IonicPage()
@Component({
	selector: 'page-admin-home',
	templateUrl: 'admin-home.html',
})
export class AdminHomePage implements OnInit {

	posts: any[];
	user: User;

	constructor(private postData: PostData,
				private storage: Storage,
				private navCtrl: NavController,
				private empresaService: EmpresaDataProvider) {
		this.user = <User> {};
		this.posts = postData.posts_cache;
	}

	ngOnInit(): void {
		this.storage.get(constants.KEY_USER).then((user: User) => {
			this.user = user;
			this.postData.postsByEmpresa(user.empresa.id).subscribe((posts: any) => {
				this.posts = posts;
			});
		});

		this.empresaService.empresaUpdate.subscribe(empresa => this.user.empresa = empresa);
	}

	viewPublish(post_id: number): void {
		this.navCtrl.push(PublishPage, {post_id: post_id});
	}

	goToConfig(): void {
		this.navCtrl.push(EmpresaConfigPage);
	}
}
