import {Component, OnInit} from '@angular/core';
import {IonicPage, Loading, LoadingController, NavController} from 'ionic-angular';
import {Storage} from "@ionic/storage";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {IEmpresa} from "../../models/empresa";
import {constants} from "../../app.constants";
import {User} from "../../models/user";
import {EmpresaDataProvider} from "../../providers/empresa-data";
import {Helpers} from "../../util/helpers";


@IonicPage()
@Component({
	selector: 'page-empresa-config',
	templateUrl: 'empresa-config.html',
})
export class EmpresaConfigPage implements OnInit {

	private user: User;
	isLoading: Loading;
	empresa: IEmpresa;
	myForm: FormGroup;

	constructor(public navCtrl: NavController,
				public empresaService: EmpresaDataProvider,
				private storage: Storage,
				public formBuilder: FormBuilder,
				private loadingCtrl: LoadingController,
				private helpers: Helpers) {

		this.isLoading = this.loadingCtrl.create({content: 'Espere por favor...'});
		this.isLoading.present();

		if (!this.empresaService.sectores) {
			this.empresaService.getSectores().subscribe();
		}
	}

	ngOnInit(): void {
		this.storage.get(constants.KEY_USER)
			.then((user: User) => {
				this.user = user;
				this.empresa = user.empresa;
				this.myForm = this.createMyForm();
				this.isLoading.dismiss();
			});
	}

	updateEmpresa(): void {
		this.isLoading = this.loadingCtrl.create({content: 'Actualizando...'});
		this.isLoading.present();

		this.empresaService.updateEmpresa(this.empresa).subscribe((empresa: IEmpresa) => {
			this.user.empresa = empresa;
			this.storage.set(constants.KEY_USER, this.user);
			this.empresaService.emitEmpresaUpdate(empresa);
			this.helpers
				.customAlert('Datos actualizados con éxito', 'Enhorabuena!')
				.present();

		}, err => {
			this.isLoading.dismiss();
		}, () => {
			this.isLoading.dismiss();
		});
	}

	private createMyForm() {
		return this.formBuilder.group({
			nombre_emp: [this.empresa.nombre_emp, Validators.required],
			correo: [this.empresa.correo, Validators.required],
			descripcion: [this.empresa.descripcion, []],
			rubro: [this.empresa.rubro, Validators.required],
			telefono: [this.empresa.telefono, Validators.required],
			direccion_emp: [this.empresa.direccion_emp, Validators.required],
			sitio_web: [this.empresa.sitio_web, []],
			fec_fundacion: [this.empresa.fec_fundacion, Validators.required],
			pcontact: [this.empresa.pcontact, Validators.required]
		});
	}
}
