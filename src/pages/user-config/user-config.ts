import {Component} from '@angular/core';
import {IonicPage, Loading, LoadingController} from 'ionic-angular';
import {UserData} from "../../providers/user-data";
import {User} from "../../models/user";
import {Storage} from "@ionic/storage";
import {constants} from "../../app.constants";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {Helpers} from "../../util/helpers";
import {PublishService} from "../../providers/publish-service";

@IonicPage()
@Component({
	selector: 'page-user-config',
	templateUrl: 'user-config.html',
})
export class UserConfigPage {

	isLoading: Loading;
	user: User;
	myForm: FormGroup;
	especialidades: IEspecialidad[];

	constructor(private userService: UserData,
				private storage: Storage,
				private formBuilder: FormBuilder,
				private loadingCtrl: LoadingController,
				private helpers: Helpers,
				private publishService: PublishService) {
		this.isLoading = this.loadingCtrl.create({content: 'Espere por favor...'});
		this.isLoading.present();
	}

	ionViewDidLoad() {
		this.checkEspecialidades();
	}

	updateUser(): void {
		this.isLoading = this.loadingCtrl.create({content: 'Actualizando...'});
		this.isLoading.present();

		this.userService.saveUser(this.user).subscribe((user: User) => {
			this.storage.set(constants.KEY_USER, user);
			this.isLoading.dismissAll();
			this.helpers.customAlert('Datos actualizados.', '').present();
			this.userService.emitUpdateUserData(this.user);
		}, err => {
			this.isLoading.dismissAll();
		});
	}

	private createMyForm(){
		return this.formBuilder.group({
			name: [this.user.name, Validators.required],
			apellidos: [this.user.apellidos, Validators.required],
			dni: [this.user.dni, Validators.required],
			email: [this.user.email, [Validators.required, Validators.email]],
			sexo: [this.user.sexo, Validators.required],
			estado_civil: [this.user.estado_civil, Validators.required],
			celular: [this.user.celular, Validators.required],
			fec_nacimiento: [this.user.fec_nacimiento, Validators.required],
			direccion: [this.user.direccion, Validators.required],
			especialidad_id: [this.user.especialidad_id, Validators.required],
		});
	}

	private checkEspecialidades(): void {
		this.publishService.checkIfEspecialidadesIsInCache().then((data: any) => {
			if (!data) {
				this.publishService.getEspecialidades()
					.subscribe(resource => {
						this.prepareData(resource);
					});
			} else {
				this.prepareData(data);
			}
		});
	}

	private prepareData(data: any): void {
		this.especialidades = data.especialidades as IEspecialidad[];

		this.storage.get(constants.KEY_USER)
			.then((user: User) => {
				this.user = user;
				this.myForm = this.createMyForm();
				this.isLoading.dismiss();
			});
	}
}

