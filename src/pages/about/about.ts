import { Component } from '@angular/core';

@Component({
	selector: 'page-about',
	templateUrl: 'about.html'
})
export class AboutPage {
	shownGroup: boolean = null;
	diseases: any[];

	constructor() {
		this.diseases = [
			{ title: "¿Dónde registrarme?", description: "Muy fácil, si eres alumno o egresado tienes que registrarte aquí http://bolsadetrabajo.unjfsc.edu.pe/registro-aspirante y si eres empleador en la siguiente dirección http://bolsadetrabajo.unjfsc.edu.pe/registro-empleador" },
			{ title: "¿Cuando me llegarán notificaciones?", description: "Para que te lleguen las notificaiones al celular se tiene que estar logeado con tus credenciales DNI y contraseña; no tener la aplicación abierta o en primer plano. La notificación llegará con una ventana informativa de forma rectangular, y este al salir emitirá un sonido de aviso!!!" },
			{ title: "¿Me avisarán si hay nueva versión?", description: "Cuando se implementen nuevas funcionalidades o se agreguen más interfaces, se estará avisando a todos los usuarios registrados, vía email, llamada telefónica o vía notificación al celular ;)" },
			{ title: "No veo todas las convocatorias", description: "La APP de Bolsa de Trabajo está optimizada para que a cada alumno o egresado le lleguen notificaciones de su interés, y por ende solo podrán visualizar convocatorias de trabajo relevantes." },
			{ title: "Objetivos alcanzados", description: "La App de bolsa de trabajo permite lograr un rápido acceso a las convocatorias de prácticas pre-profesionales, prácticas profesionales y/o laborales de las empresas y/o instituciones para  los estudiantes y egresados de la Universidad Nacional José Faustino Sánchez Carrión." }
		]
	}

	toggleGroup(group): void {
		if (this.isGroupShown(group)) {
			this.shownGroup = null;
		} else {
			this.shownGroup = group;
		}
	};

	isGroupShown(group): boolean {
		return this.shownGroup === group;
	};
}
