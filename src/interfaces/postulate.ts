export interface Postulate {
	id?: number;
	user_id: number;
	post_id: number;
	created_at?: string;
}
