import {Alert, AlertController} from "ionic-angular";
import {Injectable} from "@angular/core";

@Injectable()
export class Helpers {

	constructor(private alertCtrl: AlertController) {
	}

	public customAlert(subTitle: string, title: string = 'Error!!!'): Alert {
		return this.alertCtrl.create({
			title: title,
			subTitle: subTitle,
			buttons: ['Cerrar']
		});
	}
}
