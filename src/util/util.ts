import {HttpErrorResponse} from "@angular/common/http";
import {ErrorObservable} from "rxjs/observable/ErrorObservable";
import {Observable} from "rxjs/Observable";

export class Util {

	constructor() {
	}

	public static errorHandler(error: HttpErrorResponse): Observable<any> {

		if (error.error instanceof ErrorEvent) {
			// A client-side or network error occurred.
			console.error('Un error ha ocurrido:', error.error.message);
			return new ErrorObservable(error.error.message);
		} else {
			// The backend returned an unsuccessful response code.
			if (!error.status) {
				alert('Error desconocido, parece que no tiene conexión a internet.');
				return new ErrorObservable(null);
			} else if (error.status === 404) {
				return new ErrorObservable('El usuario o contraseña es incorrecto');
			}
		}
		return new ErrorObservable(error);
	}

	public static createErrorAlert(errorData: HttpErrorResponse, params: any): string {
		const errorsObject = errorData.error;
		let errors: any[] = [];
		let html = '<ul class="list-unstyle">';

		for (const field in params) {
			if (errorsObject.hasOwnProperty(field)) {
				errors.push(errorsObject[field]);
			}
		}

		for (let i = 0; i < errors.length; i++) {
			if (errors[i].length > 1) {
				errors[i].forEach(error => {
					html += '<li>' + error + '</li>';
				});
			} else {
				html += '<li>' + errors[i] + '</li>';
			}
		}

		html += '</ul>';

		return html
	}

	public static getCiudades(ciudades: any): any[] {
		let data = [];
		for (const ciudad in ciudades) {
			data.push({id: ciudad, name: ciudades[ciudad]});
		}

		return data;
	}
}
