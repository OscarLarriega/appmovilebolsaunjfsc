export class Convocatoria {

	id?: number;
	title: string;
	content: string;
	requisitos: string;
	email: string;
	telefono: string;
	modalidad: string;
	modo: string;
	ciudad: string;
	tags: string;
	num_vacante: number;
	sueldo: number;
	fecha_inicio: string;
	fecha_fin: string;

	// Other table
	especialidad: Array<number>;

	user_id?: number;

	constructor(title: string, content: string, requisitos: string, email: string, telefono: string,
				modalidad: string, modo: string, ciudad: string, tags: string, num_vacante: number, sueldo: number,
				fecha_inicio: string, fecha_fin: string, especialidad: Array<number>) {
		this.title = title;
		this.content = content;
		this.requisitos = requisitos;
		this.email = email;
		this.telefono = telefono;
		this.modalidad = modalidad;
		this.modo = modo;
		this.ciudad = ciudad;
		this.tags = tags;
		this.num_vacante = num_vacante;
		this.sueldo = sueldo;
		this.fecha_inicio = fecha_inicio;
		this.fecha_fin = fecha_fin;
		this.especialidad = especialidad;
	}

	public static createBlank() {
		let fecha_fin = new Date();
		fecha_fin.setMonth(fecha_fin.getMonth() + 2);

		return new Convocatoria('', '', '', '', '', '', '', '', '', null, null,
			new Date().toISOString(), fecha_fin.toISOString(), []);
	}
}
