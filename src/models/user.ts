import {IEmpresa} from "./empresa";

export class User {
	id: number;
	tipo_id: number;
	especialidad_id: number;
	name: string;
	apellidos: string;
	estado_civil: string;
	email: string;
	foto: string;
	dni: string;
	sexo: string;
	celular: string;
	direccion: string;
	is_fcm_token?: string;
	fec_nacimiento?: string;
	password?: string;

	empresa: IEmpresa;

	constructor() {
	}
}
