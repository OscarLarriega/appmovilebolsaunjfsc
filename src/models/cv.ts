export class Cv {
	id?: number;
	user_id?: number;
	perfil: string;
	ciudad: string;
	edu_primaria: string;
	fec_fin_primaria: string;
	edu_secundaria: string;
	fec_fin_secundaria: string;
	edu_superior: string;
	fec_fin_superior: string;
	exp_laboral: string;
	capacitaciones: string;
	sueldo: number;
	idiomas: string;

	constructor(perfil: string, ciudad: string, edu_primaria: string, fec_fin_primaria: string,
				edu_secundaria: string, fec_fin_secundaria: string, edu_superior: string, fec_fin_superior: string,
				exp_laboral: string, capacitaciones: string, sueldo: number, idiomas: string) {
		this.perfil = perfil;
		this.ciudad = ciudad;
		this.edu_primaria = edu_primaria;
		this.fec_fin_primaria = fec_fin_primaria;
		this.edu_secundaria = edu_secundaria;
		this.fec_fin_secundaria = fec_fin_secundaria;
		this.edu_superior = edu_superior;
		this.fec_fin_superior = fec_fin_superior;
		this.exp_laboral = exp_laboral;
		this.capacitaciones = capacitaciones;
		this.sueldo = sueldo;
		this.idiomas = idiomas;
	}

	public static createBlank() {
		return new Cv('', '', '', '', '', '', '', '', '', '', null, '');
	}
}
