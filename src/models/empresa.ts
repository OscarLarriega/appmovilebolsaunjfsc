export interface IEmpresa {
	id?: number;
	nombre_emp: string;
	direccion_emp: string;
	photo: string;
	descripcion?: string;
	rubro?: string;
	ruc: string;
	correo?: string;
	telefono?: string;
	sitio_web?: string;
	fec_fundacion?: string;
	pcontact?: string;
}

export class Empresa implements IEmpresa{

	id?: number;
	nombre_emp: string;
	direccion_emp: string;
	photo: string;
	descripcion?: string;
	rubro?: string;
	ruc: string;
	correo?: string;
	telefono?: string;
	sitio_web?: string;
	fec_fundacion?: string;
	pcontact?: string;

	constructor(model?: any) {
		if (model) {
			for (let key in model) {
				this[key] = model[key];
			}
		}
	}
}
