import { Component, ViewChild } from '@angular/core';
import {MenuController, Nav, Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from "@ionic/storage";

import {LoginPage} from '../pages/login/login';
import {AboutPage} from "../pages/about/about";
import {PublishPage} from "../pages/publish/publish";
import {CvPage} from "../pages/cv/cv";
import {HomePage} from "../pages/home/home";
import {UserData} from "../providers/user-data";
import {constants} from "../app.constants";
import {User} from "../models/user";
import {AdminHomePage} from "../pages/admin-home/admin-home";
import {UserConfigPage} from "../pages/user-config/user-config";
import {EmpresaConfigPage} from "../pages/empresa-config/empresa-config";

@Component({
	templateUrl: 'app.html'
})
export class MyApp {
	@ViewChild(Nav) nav: Nav;
	rootPage:any;
	user: User;

	constructor(platform: Platform,
				statusBar: StatusBar,
				splashScreen: SplashScreen,
				private storage: Storage,
				private menu: MenuController,
				private userService: UserData) {

		this.user = <User> {tipo_id: 0};

		this.watchUserUpdate();

		platform.ready().then(() => {
			// Okay, so the platform is ready and our plugins are available.
			// Here you can do any higher level native things you might need.
			statusBar.styleDefault();

			storage.get(constants.KEY_USER).then((user: User) => {
				if (user !== null && user.id) {
					this.user = user;
					if (user.tipo_id == constants.ASPIRANTE) {
						this.rootPage = HomePage;
					} else if (user.tipo_id == constants.EMPRESA) {
						this.rootPage = AdminHomePage;
					} else {
						this.rootPage = LoginPage;
					}
				} else {
					this.rootPage = LoginPage;
				}
			});
			splashScreen.hide();
		});
	}

	logout(): void {
		this.menu.close();
		this.storage.get(constants.KEY_USER).then((val: User) => {
			this.userService.logout(val.id).subscribe(() => {
				this.storage.remove(constants.KEY_USER);
				this.rootPage = LoginPage;
				this.nav.push(LoginPage);
			}, error => {
				alert('Hubo un error al cerrar la sesión, contacte con el area de soporte.');
			});
		});
	}

	aboutPage(): void {
		this.nav.push(AboutPage);
		this.menu.close();
	}

	publishPage(): void {
		this.nav.push(PublishPage);
		this.menu.close();
	}

	cvPage(): void {
		this.nav.push(CvPage);
		this.menu.close();
	}

	configUser(): void {
		this.nav.push(UserConfigPage);
		this.menu.close();
	}

	configEmpresa(): void {
		this.nav.push(EmpresaConfigPage);
		this.menu.close();
	}

	private watchUserUpdate(): void {
		this.userService.userUpdate.subscribe({
			next: (event: User) => this.user = event
		});
	}
}

