import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from "@ionic/storage";
import { FCM } from "@ionic-native/fcm";
import { Vibration } from "@ionic-native/vibration";

import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';
import { HomePage } from "../pages/home/home";
import { UserData } from "../providers/user-data";
import { PostData } from "../providers/post-data";
import { AboutPage } from "../pages/about/about";
import { PublishPage } from "../pages/publish/publish";
import { CvPage } from "../pages/cv/cv";
import { DetailPage } from "../pages/detail/detail";
import { EmpresaDataProvider } from '../providers/empresa-data';
import { PublishService } from "../providers/publish-service";
import { CvProvider } from '../providers/cv/cv';
import { AdminHomePage } from "../pages/admin-home/admin-home";
import { UserConfigPage } from "../pages/user-config/user-config";
import { HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import { Interceptor } from "../interceptor/http-request.interceptor";
import { Helpers} from "../util/helpers";
import {EmpresaConfigPage} from "../pages/empresa-config/empresa-config";

@NgModule({
	declarations: [MyApp, LoginPage, HomePage, AboutPage, PublishPage, CvPage, DetailPage, AdminHomePage, UserConfigPage, EmpresaConfigPage],
	imports: [
		BrowserModule,
		IonicModule.forRoot(MyApp),
		HttpClientModule,
		IonicStorageModule.forRoot()
	],
	bootstrap: [IonicApp],
	entryComponents: [MyApp, LoginPage, HomePage, AboutPage, PublishPage, CvPage, DetailPage, AdminHomePage, UserConfigPage, EmpresaConfigPage],
	providers: [
		StatusBar,
		SplashScreen,
		{provide: ErrorHandler, useClass: IonicErrorHandler},
		{provide: HTTP_INTERCEPTORS, useClass: Interceptor, multi: true},
		UserData, PostData, EmpresaDataProvider, PublishService, FCM, Vibration,
    	CvProvider, Helpers
	]
})
export class AppModule {}
