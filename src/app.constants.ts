export const constants = {

	URL_HOST: 'http://bolsadetrabajo.unjfsc.edu.pe', // 'http://bolsadetrabajo.unjfsc.edu.pe' http://bt.intuyes.com
	API_SERVER: (url_host) => `${url_host}/api`,
	KEY_ESPECIALIDADES_CIUDADES: 'data_ciudades_especialidades',
	KEY_USER: 'user',
	KEY_POSTULATIONS: 'postulations',

	ASPIRANTE: 1,
	EMPRESA: 2
};
