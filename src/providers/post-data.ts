import {Injectable} from "@angular/core";
import {constants} from "../app.constants";
import {Postulate} from "../interfaces/postulate";
import {HttpClient} from "@angular/common/http";
import {Storage} from "@ionic/storage";
import "rxjs/add/operator/map";
import {catchError} from "rxjs/operators";
import {Util} from "../util/util";
import {Observable} from "rxjs/Observable";
import {Convocatoria} from "../models/convocatoria";

@Injectable()
export class PostData {

	private _posts_cache: Convocatoria[];
	private _postulations: Postulate[];

	constructor(public http: HttpClient,
				private storage: Storage) {
	}

	posts(especialidad: number): Observable<IEspecialidad[]> {
		return this.http
			.get<IEspecialidad[]>(`${constants.API_SERVER(constants.URL_HOST)}/posts?especialidad=${especialidad}`)
			.map(res => res)
			.pipe(catchError(Util.errorHandler));
	}

	postsByEmpresa(empresa_id: number): Observable<Convocatoria[]> {
		return this.http.get<Convocatoria[]>(`${constants.API_SERVER(constants.URL_HOST)}/posts-empresa/${empresa_id}`)
			.map(res => this._posts_cache = res)
			.pipe(catchError(Util.errorHandler));
	}

	postulate(postulate: Postulate): Observable<Postulate> {
		return this.http.post<Postulate>(`${constants.API_SERVER(constants.URL_HOST)}/postulate`, postulate)
			.pipe(catchError(Util.errorHandler));
	}

	getPostulations(user_id: number): void {
		this.http
			.get<Postulate[]>(`${constants.API_SERVER(constants.URL_HOST)}/postulations/${user_id}`).map(res => {
				this.postulations = res as Postulate[];
				this.storage.set(constants.KEY_POSTULATIONS, this.postulations);
			}).subscribe();
	}

	updatePostCache(data: Convocatoria): void {
		this.posts_cache.forEach((convocatoria: Convocatoria) => {
			if (data.id == convocatoria.id) {
				convocatoria = data;
				return;
			}
		});
	}

	get posts_cache(): Convocatoria[] {
		return this._posts_cache;
	}

	set posts_cache(value: Convocatoria[]) {
		this._posts_cache.unshift(value[0]);
	}

	get postulations(): Postulate[] {
		return this._postulations;
	}

	set postulations(value: Postulate[]) {
		this._postulations = value;
	}

	pushPostulation(postulate: Postulate): void {
		this._postulations.push(postulate);
	}
}
