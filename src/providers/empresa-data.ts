import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import {constants} from "../app.constants";
import {HttpClient} from "@angular/common/http";
import {Empresa, IEmpresa} from "../models/empresa";
import {Observable} from "rxjs/Observable";
import {catchError} from "rxjs/operators";
import {Util} from "../util/util";
import {ISector} from "../interfaces/sector";
import { Subject } from 'rxjs/Subject';

@Injectable()
export class EmpresaDataProvider {

	sectores: ISector[];
	empresaUpdate: Subject<IEmpresa>;

	constructor(public http: HttpClient) {
		this.empresaUpdate = new Subject<IEmpresa>();
	}

	shortInfo(empresaID: number): Observable<Empresa> {
		return this.http.get<Empresa>(`${constants.API_SERVER(constants.URL_HOST)}/empresa/${empresaID}`)
			.pipe(catchError(Util.errorHandler));
	}

	updateEmpresa(empresa: IEmpresa): Observable<IEmpresa> {
		return this.http.put<IEmpresa>(`${constants.API_SERVER(constants.URL_HOST)}/empresa`, empresa)
			.pipe(catchError(Util.errorHandler));
	}

	public getSectores(): Observable<ISector[]> {
		return this.http.get<ISector[]>(`${constants.API_SERVER(constants.URL_HOST)}/sectores`)
			.map(res => this.sectores = res)
			.pipe(catchError(Util.errorHandler));
	}

	emitEmpresaUpdate(empresa: IEmpresa): void {
		this.empresaUpdate.next(empresa);
	}
}
