import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { Cv } from "../../models/cv";
import { constants } from "../../app.constants";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {catchError} from "rxjs/operators";
import {Util} from "../../util/util";

@Injectable()
export class CvProvider {

	constructor(public http: HttpClient) {
	}

	public createCv(cv: Cv): Promise<Cv> {
		return this.http.post(`${constants.API_SERVER(constants.URL_HOST)}/create-cv`, cv)
			.toPromise()
			.then(response => response as Cv);
	}

	public getCv(user_id: number): Observable<Cv> {
		return this.http.get<Cv>(`${constants.API_SERVER(constants.URL_HOST)}/cv/${user_id}`)
			.pipe(catchError(Util.errorHandler));
	}
}
