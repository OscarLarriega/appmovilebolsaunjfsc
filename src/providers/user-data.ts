import {EventEmitter, Injectable} from "@angular/core";
import {constants} from "../app.constants";
import {User} from "../models/user";
import {HttpClient} from "@angular/common/http";
import {Util} from "../util/util";
import {Observable} from "rxjs/Observable";
import {catchError} from "rxjs/operators";

@Injectable()
export class UserData {

	userUpdate: EventEmitter<User>;

	constructor(public http: HttpClient) {
		this.userUpdate = new EventEmitter<User>();
	}

	login(data: User): Observable<User> {
		return this.http.post<User>(`${constants.API_SERVER(constants.URL_HOST)}/login`, data)
			.pipe(catchError(Util.errorHandler));
	}

	saveToken(token: string, user: User): Observable<any> {
		return this.http.post(`${constants.API_SERVER(constants.URL_HOST)}/token`, {id: user.id, token: token})
			.pipe(catchError(Util.errorHandler));
	}

	logout(userId: number): Observable<any> {
		return this.http.post(`${constants.API_SERVER(constants.URL_HOST)}/logout`, {id: userId})
			.pipe(catchError(Util.errorHandler));
	}

	saveUser(user: User): Observable<User> {
		return this.http.put(`${constants.API_SERVER(constants.URL_HOST)}/user`, user)
			.pipe(catchError(Util.errorHandler));
	}

	emitUpdateUserData(user: User): void {
		this.userUpdate.next(user);
	}
}
