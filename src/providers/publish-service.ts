import {Injectable} from "@angular/core";
import {Storage} from "@ionic/storage";
import {constants} from "../app.constants";
import {Convocatoria} from "../models/convocatoria";
import {User} from "../models/user";
import {HttpClient} from "@angular/common/http";
import {catchError} from "rxjs/operators";
import {Util} from "../util/util";
import {Observable} from "rxjs/Observable";

@Injectable()
export class PublishService {

	user: User;

	constructor(private http: HttpClient,
				private storage: Storage) {
		this.getUser();
	}

	public getEspecialidades(): Observable<any> {
		return this.http.get<any>(`${constants.API_SERVER(constants.URL_HOST)}/especialidades`)
			.map(response => {
				this.storage.set(constants.KEY_ESPECIALIDADES_CIUDADES, response);
				return response;
			})
			.pipe(catchError(Util.errorHandler));
	}

	public checkIfEspecialidadesIsInCache(): Promise<IEspecialidad[]> {
		return this.storage.get(constants.KEY_ESPECIALIDADES_CIUDADES);
	}

	public createConvocatoria(params: Convocatoria): Observable<Convocatoria> {
		params.user_id = this.user.id;
		let http: Observable<Object>;
		const url: string = `${constants.API_SERVER(constants.URL_HOST)}/post`;

		if (params.id > 0) {
			http = this.http.put<Convocatoria>(url, params);
		} else {
			http = this.http.post<Convocatoria>(url, params);
		}

		return http.pipe(catchError(Util.errorHandler));
	}

	private getUser(): void {
		this.storage.get(constants.KEY_USER).then((user: User) => {
			this.user = user;
		});
	}
}
