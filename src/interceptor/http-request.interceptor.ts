import {Injectable} from "@angular/core";
import {
	HttpEvent,
	HttpHandler,
	HttpInterceptor,
	HttpRequest
} from "@angular/common/http";
import {Observable} from "rxjs/Observable";

@Injectable()
export class Interceptor implements HttpInterceptor {

	constructor() {
	}

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		req = req.clone({
			setHeaders: {
				'X-Requested-With': 'XMLHttpRequest'
			}
		});
		return next.handle(req);
	}
}
